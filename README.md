# Jouons avec XML et XSLT

Découverte du XSLT

## Consignes

A partir du fichier XML suivant ainsi que le fichier XSLT suivant, implémenter dans le langage de votre choix, un script permettant de transformer l'XML par le biais du fichier XSLT.

Une fois que vous êtes parvenu à le faire, ordonnez les enregistrements pour que les artistes soient listés dans l'ordre alphabétique .

```xml
<?xml version="1.0" encoding="UTF-8"?>
<catalog>
    <cd>
        <title>Empire Burlesque</title>
        <artist>Bob Dylan</artist>
        <country>USA</country>
        <company>Columbia</company>
        <price>10.90</price>
        <year>1985</year>
    </cd>
    <cd>
        <title>Toto</title>
        <artist>Toto</artist>
        <country>USA</country>
        <company>Columbia records</company>
        <price>10.56</price>
        <year>1978</year>
    </cd>
    <cd>
        <title>Nevermind</title>
        <artist>Nirvana</artist>
        <country>USA</country>
        <company>DGC records</company>
        <price>9.2</price>
        <year>1991</year>
    </cd>
    <cd>
        <title>12 bar bruise</title>
        <artist>King Gizzard and the Lizard Wizard</artist>
        <country>USA</country>
        <company>Flightess</company>
        <price>15.2</price>
        <year>2012</year>
    </cd>
    <cd>
        <title>From here to there</title>
        <artist>Girls in Hawaii</artist>
        <country>Belgium</country>
        <company>China Shop Music</company>
        <price>13</price>
        <year>2005</year>
    </cd>
    <cd>
        <title>Applause</title>
        <artist>Balthazar</artist>
        <country>Belgium</country>
        <company>Munich Records</company>
        <price>15</price>
        <year>2010</year>
    </cd>
</catalog>
```

```xml
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
    <body>
      <h2>My CD Collection</h2>
      <table border="1">
        <tr bgcolor="#9acd32">
          <th>Title</th>
          <th>Artist</th>
        </tr>
        <xsl:for-each select="catalog/cd">
          <xsl:if test="price &gt; 10">
          <tr>
            <td><xsl:value-of select="title"/></td>
            <td><xsl:value-of select="artist"/></td>
          </tr>
          </xsl:if>
        </xsl:for-each>
      </table>
    </body>
  </html>
</xsl:template>
</xsl:stylesheet>
```

